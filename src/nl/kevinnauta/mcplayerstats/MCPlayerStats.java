package nl.kevinnauta.mcplayerstats;

/**
 * @author Diamundo
 * Github: https://github.com/Diamundo/MCPlayerStats
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class MCPlayerStats {
	static BufferedReader br;
	static File wd;
	private static List<Player> players;
	
	//Settings
	private static boolean showFancy		= true;
	private static boolean hideUUID			= true;
	private static boolean showTicks		= false;
	private static boolean showSeconds		= false;
	private static boolean showMinutes		= false;
	private static boolean showHours		= false;
	private static boolean showDays			= false;
	private static boolean showFull			= false;
	private static boolean showClean		= false;
	private static boolean showLargest		= true;
	
	public static void main(String[] args) {
		processArguments(args);
		
		wd = new File(System.getProperty("user.dir"));
		
		if(showFancy) { printServerInfo(); }
		players = new ArrayList<>();
		loadUUIDs();		//from wd/usernamecache.json
		loadPlayTimes();	//from wd/world/stats/<UUID>.json
		loadLastLogins();	//from wd/logs/*.log.gz
		
		//Sort all players by playtime and print
		players.sort(new Comparator<Player>() {
			@Override
			public int compare(Player o1, Player o2) {
				Integer p1 = o1.getPlayTime();
				Integer p2 = o2.getPlayTime();
				return p1.compareTo(p2) * -1;
			}
        });
		
    	for(Player p : players) {
			System.out.println(UUIDtoName(p.getUUID()) + " played " + formatPlayerTime(p.getPlayTime()) + ", last login at " + p.getLastLogin().getTime().toString());
		}
		
	}
	
	private static void processArguments(String[] args) {
		int shows = 0;
		for(int i=0; i<args.length; i++) {
			switch(args[i]) {
				case "-?" : case "--help" :
					showHelp();
					System.exit(0);
					break;
				
				case "-u": case "--UUID":
					hideUUID = false;
					break;

				case "-r": case "--raw":
					showFancy = false;
					break;
					
				case "-t" : case "--ticks ":
					showTicks = true;
					showLargest = false;
					shows++;
					break;

				case "-s": case "--seconds":
					showSeconds = true;
					showLargest = false;
					shows++;
					break;

				case "-m": case "--minutes":
					showMinutes = true;
					showLargest = false;
					shows++;
					break;

				case "-h": case "--hours":
					showHours = true;
					showLargest = false;
					shows++;
					break;

				case "-d": case "--days":
					showDays = true;
					showLargest = false;
					shows++;
					break;
				
				case "-c": case "--clean":
					showClean = true;
					showLargest = false;
					shows++;
					break;
					
				case "-f": case "--full":
					showFull = true;
					showLargest = false;
					shows++;
					break;

				case "-l": case "--largest":
					showLargest = true;
					shows++;
					break;
					
				default:
					System.err.println("WARNING: unhandled argument '" + args[i] + "'!");
					break;
			}
		}
		if(shows>1) { System.err.println("ERROR: -f/-t/-s/-m/-h/-d/-l are mutually exclusive!"); System.exit(0); }
	}
	
	private static void printCopyright() {
		System.out.println("MCPlayerStats @ " + DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").format( LocalDateTime.now() ) );
		System.out.println("Github: https://github.com/Diamundo/MCPlayerStats - (C) today.year()\n");
	}
	
	private static void showHelp() {
		printCopyright();
		System.out.println("How to use:");
		System.out.println("java -jar mcplayerstats.jar [arg]\n");
		//
		System.out.println("Possible args:");
		System.out.println("  -? / --help       : Prints this help menu, then quits the program.");
		System.out.println("  -x / --export     : Prints the playtime as a Comma-separated-value list."); //TODO
		System.out.println("  -r / --raw        : Only prints the list of players and their playtime and last login, nothing else.");
		System.out.println("  -u / --UUID       : Prints the UUID instead of the Username (default is username).");
		System.out.println("  -f / --full       : Prints playtime with all units.");		
		System.out.println("  -c / --clean      : Prints playtime with all non-zero units.");
		System.out.println("  -t / --ticks      : Prints playtime as number of ticks.");
		System.out.println("  -s / --seconds    : Prints playtime as number of seconds.");
		System.out.println("  -m / --minutes    : Prints playtime as number of minutes.");
		System.out.println("  -h / --hours      : Prints playtime as number of hours.");
		System.out.println("  -d / --days       : Prints playtime as number of days.");
		System.out.println("  -l / --largest    : Prints playtime as largest unit (default).");

		System.out.println("Warning: -f/-c/-t/-s/-m/-h/-d/-l are mutually exclusive!"); //TODO exclude: -f -t => full except ticks
	}
	
	private static void loadUUIDs(){
		String line;
		try {
			br = new BufferedReader( new FileReader(wd.getAbsolutePath() + "/usernamecache.json") );
			while( (line = br.readLine()) != null) {
				if(line.contains(":")) {
					line = line.replace("\"", "").replace(" ", "").replace(",", "");
					MCPlayerStats.players.add( new Player( line.substring(0, line.indexOf(":")), line.substring(line.indexOf(":") + 1, line.length()) ) );
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void loadPlayTimes(){
		String file = null;

		List<File> files = (List<File>) Arrays.asList(new File(wd + "/world/stats/" ).listFiles() );
		for(File f : files) {
			if(f.getPath().endsWith(".json")) {
				try {
					br = new BufferedReader( new FileReader(f.getAbsolutePath()) );
					file = br.readLine();
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if(file.contains("playOneMinute")) {
					file = file.substring(file.indexOf("playOneMinute") + 15 , file.length());
					file = file.substring(0, file.indexOf(","));
				}

				for(Player p : players) {
					if(p.getUUID().equals( f.getName().substring(0, f.getName().length() - 5) )) {
						//compare the UUID with the filename, which is also the UUID + '.json'
						p.setPlayTime(Integer.parseInt(file)); //now contains only the string value of stat.playOneMinute
						break;
					}
				}//end of forloop players
				
			}
		}//end of forloop directory
		
	}
	
	private static void loadLastLogins(){
		ArrayList<String> file = new ArrayList<>();
		String log = null;

		List<File> files = (List<File>) Arrays.asList(new File(wd + "/logs/" ).listFiles() );
		Collections.sort(files, Collections.reverseOrder()); //Sort backwards, so most recent log is first
		//TODO NICE: Also go through latest.log
		
		for(File f : files) {
			if(f.getPath().endsWith(".log.gz")) {
				try {
					
					InputStream fileStream = new FileInputStream(f.getAbsolutePath());
					InputStream gzipStream = new GZIPInputStream(fileStream);
					Reader decoder = new InputStreamReader(gzipStream);
					BufferedReader br = new BufferedReader(decoder);

					while( (log = br.readLine()) != null) {
						file.add(log);
					}
				
					br.close(); decoder.close(); gzipStream.close(); fileStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				for(String line : file) {
					if(line.contains("[User Authenticator")) { //Actual login line, containing both Name and UUID
						String time = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
						String name = line.substring(line.indexOf("UUID of player "), line.indexOf(" is "));
						String UUID = line.substring(line.indexOf(" is ") + 4, line.length());

						for(Player p : players) {
							if(p.getUUID().equals( UUID )) {
								Calendar cal = Calendar.getInstance();
								cal.set(
									Integer.parseInt(f.getName().substring(0, 4)), 		//year
									Integer.parseInt(f.getName().substring(5, 7)) - 1, 	//month [0,11]
									Integer.parseInt(f.getName().substring(8, 10)),		//day
									Integer.parseInt(time.substring(0, 2)), 			//hour
									Integer.parseInt(time.substring(3, 5)), 			//minute
									Integer.parseInt(time.substring(6, 8))  			//seconds
								);
								if(p.getLastLogin() == null || p.getLastLogin().getTimeInMillis() < cal.getTimeInMillis() ) {
									p.setLastLogin(cal);
								}
								if(p.getName() == null || p.getName().equals("")) {
									p.setName(name); //should the name not have been loaded from username.cache, do it here.
								}
								break;
							}
						}//end of forloop layers
						
					}
				}//end of forloop file
				
			}
		}//end of forloop directory
		
	}
	
	private static void printServerInfo() {
		ArrayList<String> settings = new ArrayList<>();
		String line;
		try {
			br = new BufferedReader( new FileReader(wd.getAbsolutePath() + "/server.properties") );
			while( (line = br.readLine()) != null) {
				settings.add(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		printCopyright();

		//TODO fancy server info
		System.out.println("fancy");
		System.out.println("SERVER INFO");
		System.out.println("Directory: " + wd.toString());
		System.out.println("fancy\n");
	}
	
	private static String UUIDtoName(String UUID) {
		if(!hideUUID) { return UUID; }
		for(Player p : players) {
			if(UUID.equals( p.getUUID() )) { return p.getName(); }
		}
		return "UNKNOWN UUID";
	}
	
	private static String formatPlayerTime(int ticks) {
		String line = "";
		
		if(showDays || showFull || (showLargest && ticks >= 20*60*60*24) || (showClean && ticks >= 20*60*60*24)) {
			line += ticks / (20*60*60*24) + " days, ";
			if(showFull || showClean) { ticks %= (20*60*60*24); }
		}
		if(showHours || showFull || (showLargest && ticks >= 20*60*60 && ticks < 20*60*60*24) || (showClean && ticks >= 20*60*60)) {
			line += ticks / (20*60*60) + " hours, ";
			if(showFull || showClean) { ticks %= (20*60*60); }
		}
		if(showMinutes || showFull || (showLargest && ticks >= 20*60 && ticks < 20*60*60 ) || (showClean && ticks >= 20*60)) {
			line += ticks / (20*60) + " minutes, ";
			if(showFull || showClean) { ticks %= (20*60); }
		}
		if(showSeconds || showFull || (showLargest && ticks >= 20  && ticks < 20*60) || (showClean && ticks >= 20)) {
			line += ticks/20 + " seconds, ";
			if(showFull || showClean) { ticks %= 20; }
		}
		if(showTicks || showFull || (showLargest && ticks < 20) || (showClean && ticks < 20 )) {
			line += ticks + " ticks, ";
		}
		line = line.substring(0, line.length()-2);
		return line;
	}

}

final class Player {
	private String UUID;
	private String Name;
	private int PlayTime; //in ticks
	private Calendar LastLogin;
	public  String getUUID() { return UUID; }
	public  String getName() { return Name; }
	public  int getPlayTime() { return PlayTime; }
	public  Calendar getLastLogin() { return LastLogin; }
	
	public Player() {
		this.UUID = "";
		this.Name = "";
		this.PlayTime = 0;
		this.LastLogin = null;
	}	
	
	public Player(String UUID, String Name) {
		this.UUID = UUID;
		this.Name = Name;
	}

	public void setName(String name) { this.Name = name; }
	public void setPlayTime(int ticks) { this.PlayTime = ticks; }
	public void setLastLogin(Calendar login) { this.LastLogin = login; }
	
	public String toString() { return "Player<" + getUUID() + "," + Name + "," + PlayTime + "," + LastLogin.getTime().toString() + ">";}
}


