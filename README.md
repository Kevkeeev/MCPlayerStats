# MCPlayerStats

Tool to show all players that have been on a server, and how long they have been playing, in human-readable format.

## How to use:
1. Download mcplayerstats.jar
2. Upload mcplayerstats.jar to your server's home folder (where the world map is located)
3. Execute the jar by doubleclicking or entering the command `java -jar mcplayerstats.jar`

## Issues? Improvements?
Open an issue, and I'll get back to you!
